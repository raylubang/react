import React, { Component } from 'react'; 
import { Media } from 'reactstrap' ; 
import { Card, CardImg, CardImgOverlay, CardText, CardBody,CardTitle } from 'reactstrap';   

class Dishdetail extends Component {     
    constructor(props) {         
        super(props) ;          
        this.state = {              
            //This is implemented to click event to take the dish page              
            //selectedDish : null               
            selectedDish : null          
        }      
    }      
    onDishSelect(dish) {         
        //when user chooses a dish update state to "selectedDish to currebt dish"         
        this.setState({ selectedDish: dish })     
    }      
    renderDish(dish) {         
        // make sure the selected dish is an existing dish         
        if(dish != null) {               
            return (                   
            <Card>                        
                <CardImg width="100%" src={dish.image} alt={dish.name} />                        
                <CardBody>                           
                    <CardTitle>{dish.name}</CardTitle>                           
                    <CardText>{dish.description}</CardText>                                          
                </CardBody>                   
            </Card>               
            )         
        }        
         else {             
             return (               
             <div></div>             
             );         
        }     
    }      
    // This function renders the selectedDish and displays below     
    //<div className="row">       
    //  {this.renderDish(this.state.selectedDish)}     
    //</div>      
    render() {         
        return (           
            <div className="col-12 col-md-5 m-1">               
            <div className="container">                   
            <div className="row">                         
            {this.renderDish(this.state.selectedDish)}                    
            </div>               
            </div>              
            </div>         
            )     
        } 
    } export default Dishdetail; 